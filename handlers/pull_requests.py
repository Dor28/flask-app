import os, json
from urllib import response
import requests
from dotenv import load_dotenv


load_dotenv()
TOKEN = os.getenv("TOKEN")
HEADERS = {'Authorization': f'Bearer {TOKEN}'}


def label_finder(p, label_value):
        matching = False
        for label in p["labels"]:
            if label["name"] == label_value:
                matching = True
        return matching

def get_list_for_all(state):
    r = requests.get("https://api.github.com/repos/boto/boto3/pulls",headers=HEADERS, params = {"state": "all",    "per_page": 100} )
    if r.status_code == 200:        
        if state == "open":            
            return [p for p in r.json() if p["state"] == "open"]       
        elif state == "closed":            
            return [p for p in r.json() if p["state"] == "closed"]        
        elif state == "bug":            
            return [p for p in r.json() if p["state"] == "open" and label_finder(p, "bug")]        
        elif state == "needs-review":            
            return [p for p in r.json() if p["state"] == "open" and label_finder(p, "needs-review")]
def get_pull_requests(state):
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """
    print("State:", state )   
    all_info = get_list_for_all(state)    
    if all_info:        
        answer = [{"title": p["title"], "num": p["number"], "link": p["html_url"]} for p in all_info]        
        return answer   
    else:        
         return []